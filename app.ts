import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { StoreModule } from '@ngrx/store';

import { PageComponent } from './page';
import { Welcome } from './welcome/welcome';
import { ArchetypeCard } from './characterSelect/characterEntry/characterEntry';
import { CharacterSelect } from './characterSelect/characterSelect';
import { CharacterList } from './characterSelect/characterList/characterList';
import { CharacterListView } from './characterSelect/characterList/characterListView';

import { selectedFilter } from "./state";
const appRoutes: Routes = [
  { path: '', redirectTo: 'welcome', pathMatch: 'full' },
  { path: 'welcome', component: Welcome },
  { path: 'selection', component: CharacterSelect }
];

@NgModule({
  declarations: [
    Welcome,
    ArchetypeCard,
    CharacterSelect,
    PageComponent,
    CharacterList,
    CharacterListView
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    ReactiveFormsModule,
    HttpModule,
    StoreModule.provideStore({selectedFilter})
  ],
  bootstrap: [PageComponent]
})
export class AppModule { }
