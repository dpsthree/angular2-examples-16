import { Component, Input, Output, EventEmitter } from '@angular/core';

import {ICharacterEntry} from '../characterSelectService';
@Component({
  selector: 'archetype-card',
  templateUrl: './characterSelect/characterEntry/characterEntry.html'
})
export class ArchetypeCard {
  @Input() characterDetails: ICharacterEntry;
  @Input() buttonEnabled: boolean;
  @Output() characterSelection = new EventEmitter<ICharacterEntry>();

  select(mouseEvent: MouseEvent) {
    this.characterSelection.emit(this.characterDetails);
  }
}
