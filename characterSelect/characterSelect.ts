import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { ICharacterEntry, CharacterSelectService } from './characterSelectService';
import { IAppState, SET_SELECTION } from '../state';

@Component({
  selector: 'character-select',
  templateUrl: './characterSelect/characterSelect.html',
  providers: [CharacterSelectService]
})
export class CharacterSelect {
  characterNameGroup: FormGroup;
  selected: string;
  selectedCharacter: ICharacterEntry;
  selectCharacter(character: ICharacterEntry) {
    this.selectedCharacter = character;
  }

  constructor(fb: FormBuilder, private store: Store<IAppState>) {
    store.select(s => s.selectedFilter).subscribe(newSF => {
      console.log("observed a change", newSF);
      this.selected = newSF;
    });

    this.characterNameGroup = fb.group({
      characterName: ['', Validators.required]
    });
  }

  updateSelected(newSelected: string) {
    this.store.dispatch({ type: SET_SELECTION, payload: newSelected });
  }
}
