import { Component, Input, Output, EventEmitter } from '@angular/core';

import { ICharacterEntry } from '../characterSelectService';

@Component({
  selector: 'character-list-view',
  templateUrl: './characterSelect/characterList/characterListView.html'
})
export class CharacterListView {
  @Input() archetypes: ICharacterEntry[];
  @Input() buttonEnabled: boolean;
  @Output() characterSelection = new EventEmitter<ICharacterEntry>();
}
