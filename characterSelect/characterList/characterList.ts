import { Input, Component, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

import { ICharacterEntry, CharacterSelectService } from '../characterSelectService';

@Component({
  selector: 'character-list',
  templateUrl: './characterSelect/characterList/characterList.html'
})
export class CharacterList {
  @Input() buttonEnabled: boolean;
  @Output() characterSelect = new EventEmitter<ICharacterEntry>();
  archetypes$: Observable<ICharacterEntry[]>;

  constructor(charSelectService: CharacterSelectService) {
    this.archetypes$ = charSelectService.characterData();
  }

}
