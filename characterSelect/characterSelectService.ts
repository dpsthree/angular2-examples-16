import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

export interface ICharacterEntry {
  'name': string;
  'birthday': string;
  'weight': string;
  'height': number;
  'strength': number;
  'dexterity': number;
  'hair': string;
  'race': string;
  'sex': string;
  'statPref': string;
}

@Injectable()
export class CharacterSelectService {
  constructor(private http: Http) { }

  characterData() {
    return this.http.get('/api/archetypes')
      .map(res => res.json());
  }
}
