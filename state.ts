import { Action } from '@ngrx/store';

export const SET_SELECTION = 'SET_SELECTION';

export interface IAppState {
  selectedFilter: string;
}

export const selectedFilter =
  (sf: string = 'human', action: Action) => {
    switch (action.type) {
      case SET_SELECTION:
        return action.payload;
      default:
        return sf;
    }
  };
